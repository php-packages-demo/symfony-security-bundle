# symfony/[security-bundle](https://packagist.org/packages/symfony/security-bundle)

![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/security-bundle)
Infrastructure for authorization systems
https://symfony.com/components/Security

# Other demo, this one is depending on
* symfony/maker-bundle
  * [php-packages-demo/symfony-maker-bundle](https://gitlab.com/php-packages-demo/symfony-maker-bundle)
  * This has to be moved to php-packages-demo/symfony-security-bundle!!

# Official documentation
*  Symfony5: The Fast Track [*Step 15: Securing the Admin Backend*
  ](https://symfony.com/doc/current/the-fast-track/en/15-security.html)

# Unofficial documentation
* [*Using the SecurityBundle in Symfony 6*
  ](https://wouterj.nl/2021/12/security-winterworld21)
  2021-12 Wouter de Jong
* [*Implement traditional auth system in Symfony with less code than ever*
  ](https://dev.to/bornfightcompany/implement-traditional-auth-system-in-symfony-with-less-code-than-ever-5h25)
  2021-09 Goran Hrženjak
* [Symfony 4.4 Security Cheat Sheet
  ](https://medium.com/@andreiabohner/symfony-4-4-security-cheat-sheet-5e9e75fbacc0)
  2019-06 Andréia Bohner
  * [Powerful and complete Security for your apps! Symfony 4.4
    ](http://assets.andreiabohner.org/symfony/sf44-security-cheat-sheet.pdf)

## Tuturials
* [*Authentication with VueJs using Symfony and Api Platform – Part 1*
  ](https://stefanoalletti.wordpress.com/2019/08/02/authentication-with-vuejs-using-symfony-and-api-platform-part-1/)
  2019-08 Stefano Alletti
* [*Why you don’t need JWT*](https://jolicode.com/blog/why-you-dont-need-jwt)
  2019-06
  [gregoire-pineau](https://jolicode.com/equipe/gregoire-pineau)
